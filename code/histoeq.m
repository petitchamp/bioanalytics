function image=histoeq(image,methodp)
% Function : histoeq.m
% Author : SONG Ke
% Date : 1/2008
% Description : cette receoir une image et calculer le image enforce, selon
%               la methode choisie
% Argument : image: image source �� renforcer 
%            methodp: type de renforcement 
%                      il y a 3 methodes : histeq,adjust,adapt               
% Copyright 2007-2008 LAGIS 
% See also histeq,imadjust,adapthisteq

histg=zeros(256,1);
for  i =1:256
histg(i)=i-1;
end
switch methodp
    case 'histeq'
        image=histeq(image(:,:,1),histg);
    case 'adjust'
        image=imadjust(image);
    case 'adapt'
        image=adapthisteq(image);
end