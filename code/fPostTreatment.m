function strReturnData=fPostTreatment(strHandles)



%input validation test 
if(isempty(strHandles.Corrl))
    disp('Corrleation vector vide');
    return;
end
%end test

LengthVideo = size(strHandles.Corrl,2);
fftCorrl=FFTCorrelation(strHandles.Corrl);% transformation fft du vector d'intercorrelation
step=1/LengthVideo;
axefreq=(0:step:0.5-step)*strHandles.Fe; % vector de frequence pour affichage

%test of seuil value can't be too big that the sum of indice of zero and seuil
%depass the length of the video

strReturnData=peakdet(fftCorrl,axefreq,strHandles.SeuilRupture,strHandles.SeuilAmplitude);
strReturnData.sFrequence=num2str(strReturnData.frequence);
strReturnData.Periode=compteperiode(strHandles.Corrl,strHandles.Fe,strHandles.derivation);
strReturnData.nombrePeriode=size(strReturnData.Periode,2);
strReturnData.periode_moyenne=periode_moy(strReturnData.Periode);
strReturnData.axefreq=axefreq;
strReturnData.fftCorrl=fftCorrl;
strReturnData.BattementParMinute=60*1000/strReturnData.periode_moyenne;