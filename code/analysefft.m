function returndata=analysefft(hObject,handles)
%     Function : analysefft.m
%     Author : Abdelatif Serir et SONG Ke
%     Date : 11/2007
%     Description : cette fonction recevoie le handles de la GUI et
%                   proc��de l'analyze fr��quence de vid��o
%     Argument : handles    Type: struct, le handles de la GUI interface.m
% Copyright 2007-2008 LAGIS USTL


% ----------------------------------
% d��claration et initialisation des variables 
% ----------------------------------
clc;
Fe=handles.Fe; %FPS de vid��o

% -------------------------------------------------------------------------
% cette parite afficher une image pour que le utilisateur puisse chiosir la
% zone d'activit�� du coeur du larve
% -------------------------------------------------------------------------
    if (handles.AutoselectRef)                         %si la case a cocher d'Automatic Seletion of Reference est coche,la trame de reference sera choisie automatiquement
        handles.RefFrame=mimage_moyenne(handles.mov);
    end
        figure(1);%delete the figure 1
        subplot(1,2,1);
        delete(gca);
        subplot(1,2,2);
        delete(gca);
    % subplot(mov(RefFrame).cdata);
    imshow(handles.mov(handles.RefFrame).cdata);%couper l'image par la zone choisie
    [Im,ZoneSelected]=imcrop(handles.mov(handles.RefFrame).cdata);
    subplot(2,1,1);
    subimage(handles.mov(handles.RefFrame).cdata);  
    subplot(2,1,2);
    subimage(Im);
    
    %  visualiser la s��quence, si la case �� cocher "afficher les s��quence pendant analyze"
    %  dans l'interface est coch��e
    if (handles.afficheSeq)
        figure(1);
        for i = 1:(handles.LengthVideo-1)
            Im2=imcrop(handles.mov(i).cdata,ZoneSelected); 
            figure(1);
            subplot(2,1,2);
            subimage(Im2);
            pause(handles.TempsPause); % temps de delais
        end
    end

% -------------------------------------------------------------------------
% cette parite calculer inter-corr��lation entre chacune trame de la
% s��qunecne et la trame de r��f��rence 
% -------------------------------------------------------------------------
Corrl=CorrelationImage(handles.mov,ZoneSelected,handles.RefFrame);
if(handles.mediansize~=1) %filtrage median
    Corrl=medfilt1(Corrl,handles.mediansize);    
end


fftCorrl=FFTCorrelation(Corrl);% transformation fft du vector d'intercorrelation
axefreq=(1:handles.LengthVideo)/(handles.LengthVideo)*Fe-(Fe/2); % vector de frequence pour affichage

i=find(axefreq>0,1); %i is the first element bigger than 0 in the vector 
partiePositive=VectFFT((i+SeuilRupture):size(VectFFT,2));% seuil est pour eliminer les peaks pr��s de z��ro


if(~isempty(partiePositive))
  frequence=peakdet(partiePositive,axefreq,handles.LengthVideo-size(partiePositive,2),handles.SeuilAmplitude);

strfrequence=num2str(frequence);
end

periode=compteperiode(Corrl,Fe,handles.derivation);
periode_moyenne=periode_moy(periode);
Anomalie=detect_anomalie(fftCorrl,axefreq,Fe,handles.SeuilRupture,handles.SeuilAmplitude);


% -------------------------------------------------------------------------
% affichage des figure
% -------------------------------------------------------------------------
fig1=figure(1);
  close(fig1);
  figure(1);
    subplot(2,3,[2 3]);
    % plot(axetemps,Corrl);
    plot(Corrl);
    xlabel('Trame(s)');
    ylabel('Coefficent Correlation');
    grid on;
    hold on;
    plot(Corrl,'.');
    

    subplot(2,3,[5 6]);
    % plot(axetemps,Corrl,'+');
    % xlabel('Temps (s)');
    plot(axefreq,fftCorrl);
    grid on;
    if(isempty(partiePositive))
    xlabel('Erreur');
    else
    xlabel(strcat('Les fr��quences sont: ',strfrequence,'Hz'));
    end
    grid on;

%afficher les periode
    subplot(2,3,4);
    hist(periode,size(periode,2));%histograme de distribution de duree de periode
    title(strcat('Distribution des periodes '));
    xlabel(strcat('Periode (ms) moyenne:',num2str(periode_moyenne),' Periode Totale:',num2str(size(periode,2))));%afficher la duree moyenne et le nombre de periode dans le video
    ylabel('Population');
    grid on;
    
    
%--------------------------------------------------------------------------
%---------------save the datum in excel file
%--------------------------------------------------------------------------
if(1) % save the datum and figures in the files 
    handles.Repertoire=strcat(handles.Repertoire,'resultat\');
    if(~exist(handles.Repertoire,'dir')) %check if the directory exist
        [status,message,messageid]=mkdir(handles.Repertoire);
        if(~status)  % if the creation failed, quit
            disp('matlab erreur message:',message,'matlab erreur ID:',messageid);
            return;
        end
    end
    xlswrite(strcat(handles.Repertoire, handles.AviFile,'.xls'),periode_moyenne,'Sheet1','A5');
    xlswrite(strcat(handles.Repertoire, handles.AviFile,'.xls'),'ms','Sheet1','B5');
    xlswrite(strcat(handles.Repertoire, handles.AviFile,'.xls'),size(periode,2),'Sheet1','A6');
    
    if (Anomalie==1)
        xlswrite(strcat(handles.Repertoire, handles.AviFile,'.xls'),'O','Sheet1','A7');
    else
        
        xlswrite(strcat(handles.Repertoire, handles.AviFile,'.xls'),'N','Sheet1','A7');
    end
    saveas(fig1,strcat(handles.Repertoire, handles.AviFile,'_periode_frequence.fig'));
    saveas(fig2,strcat(handles.Repertoire, handles.AviFile,'_distribution_periode.fig'));
end
returndata.strfrequence=strfrequence;
returndata.periode_moyenne=periode_moyenne;
returndata.nombreperiode=size(periode,2);
returndata.Anomalie=Anomalie;