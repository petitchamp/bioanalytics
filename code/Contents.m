% MATLABFILE FINAL
%
% Files
%   analysefft       - Function : analysefft.m
%   compteperiode    - Function : CorrelationImage.m
%   CorrelationImage - Function : CorrelationImage.m
%   derivation       - 
%   detect_anomalie  - Function : detect_anomalie.m
%   dispCurrentFrame - function : dispCurrentFrame.m
%   FFTCorrelation   - Function : FFTCorrelation.m
%   histoeq          - Function : histoeq.m
%   IndiceZero       - Function : IndiceZero.m
%   interface        - M-file for interface.fig
%   kernelcal        - 
%   mimage_moyenne   - Function : mimage_moyenne.m
%   peakdet          - Function : peakdet.m
%   periode_moy      - Function : periode_moy.m
%   savedata         - 
