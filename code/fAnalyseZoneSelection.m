function ZoneSelected = fAnalyseZoneSelection(RefFrame)
%     Function : analysefft.m
%     Author : SONG Ke
%     Date : 03/2008
%     Description : This function receive a frame of video and display the frame to let the user to select the zone to be analysed
%		    and return the corrdinate of the zone
%     Argument : 
% Copyright 2007-2008 SONG Ke

    figure(1);%delete the figure 1
        subplot(1,2,1);
        delete(gca);
        subplot(1,2,2);
        delete(gca);
    % subplot(mov(RefFrame.cdata);
    imshow(RefFrame.cdata);%couper l'image par la zone choisie
    ZoneWellSelected=0;
    while(~ZoneWellSelected)
        [Im,ZoneSelected]=imcrop(RefFrame.cdata);
        if(ZoneSelected(3)*ZoneSelected(4)>1000)  % the surface of the selected zone should be larger than 100 in order to avoid mis-selection
            ZoneWellSelected=1;
        end
    end
    
    subplot(2,3,1);
    imshow(RefFrame.cdata);
    fRectangle(ZoneSelected);% draw a rectangle of the selected zone
