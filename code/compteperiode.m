function periode=compteperiode(signal,Fe,derivation)
% Function : compteperiode.m
% Author : Abdelatif Serir et SONG Ke
% Date : 11/2007
% Description : cette fonction cherche les periodes dans un signal
%               sinusoidal
%               il y a 2 methode: 1. comptage par derivation de signal mais
%                                    marche que avec les videos de bas
%                                    frequence d'echantillonnage
%                                 2. comptage par detection de passage par zero 
%                           
% Argument : signal         signal sinusoidal �� calculer le nombre de periode
%            derivation     parametre pour choisir la methode de comptage
% Copyright 2007-2008 LAGIS 

j=1;
% if (signal(1)~=0)
%     k=sign(signal(1));% prendre le signe de la 1er valeur pour commence le comptage d��s le 1er periode
% end
if (derivation)
    signal=diff(signal);% deriver le signal si on compte en utilisant la methode 1
end

for i=1: (size(signal,2)-1)
     if (signal(i)>=0) && (signal(i+1)<0)  %si il y a passage de positif a negatif, on sauvgarde l'indice du point
        indicePeriode(j)=i;
        j=j+1;
        i=i+1;
     end
end
periode=diff(indicePeriode)*(1/Fe)*1000;%calcul l'interval de periode 