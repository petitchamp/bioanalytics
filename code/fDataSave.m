function status=fDataSave(handles,strResult)
%--------------------------------------------------------------------------
%---------------save the datum in excel file
%--------------------------------------------------------------------------
if(1) % save the datum and figures in the files 
    handles.Repertoire=strcat(handles.Repertoire,'resultat\');
    status=chkandmkdir(handles.Repertoire);
if(~status)
    disp('figExport.m :path check error');
    return;
end
    xlswrite(strcat(handles.Repertoire, handles.AviFile,'.xls'),strResult.periode_moyenne,'Sheet1','A5');
    xlswrite(strcat(handles.Repertoire, handles.AviFile,'.xls'),'ms','Sheet1','B5');
    xlswrite(strcat(handles.Repertoire, handles.AviFile,'.xls'),size(periode,2),'Sheet1','A6');
    
    if (Anomalie==1)
        xlswrite(strcat(handles.Repertoire, handles.AviFile,'.xls'),'O','Sheet1','A7');
    else
        
        xlswrite(strcat(handles.Repertoire, handles.AviFile,'.xls'),'N','Sheet1','A7');
    end
    
    %save the figure file
    fig1=figure(1);
    saveas(fig1,strcat(handles.Repertoire, handles.AviFile,'_periode_frequence.fig'));
end