function Correlation=CorrelationImage(mov,ZoneSlec,RefFrame)
% Function : CorrelationImage.m
% Author : Abdelatif Serir et SONG Ke
% Date : 11/2007
% Description : cette fonction recevoie la s��quence, la zone de teste
%               choisie par utilisateur,et le num��ro de la trame de
%               r��f��rence et renvoie un vector qui contient
%               l'inter-corr��lation entre la trame de r��f��nce et chaqune
%               trame dans la s��quence , et �� la fin elle enleve la moyenne
%               des coefficient de correlation
% Argument : mov        Type:struct,la sequence de vid��o acquise
%            ZoneSlec   Type:Matice 1x4,la coordonn�� de la zone active du coeur
%                       de larve
%            RefFrame   Type:double, le num��ro de trame de r��fence choisi
%                       par utilisateur
% Copyright 2007-2008 LAGIS 

PermierIm=imcrop(mov(RefFrame).cdata,ZoneSlec);%charger la trame de r��f��rence
PermierImIntens=histoeq(PermierIm(:,:,1),'adjust');%convertir la trame de r��f��rence �� une matrice calculable
length=size(mov,2);% �� changer de periode de test

% correlation d'image de sequence et d'image de reference
Correlation=zeros(1,length);
% j=1;
for i=1:length
    CompareIm=imcrop(mov(i).cdata,ZoneSlec);
    CompareImIntens=histoeq(CompareIm(:,:,1),'adjust');
    Correlation(i)=corr2(PermierImIntens,CompareImIntens);% calculer l'inter-corr��lation entre trame
%     j=j+1;
end
Correlation=Correlation-mean(Correlation);% enlever la moyenne des signaux