function varargout = interface(varargin)
% INTERFACE M-file for interface.fig
%      GUI de analyse cadence de larve
%      Author : SONG Ke et Abdellatif Serir
%      Date : 11/2007
% Copyright 2007-2008 LAGIS 
% Begin initialization code - DO NOT EDIT

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @interface_OpeningFcn, ...
                   'gui_OutputFcn',  @interface_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin & ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% End initialization code - DO NOT EDIT
%==========================================================================

% --- Executes just before interface is made visible.
function interface_OpeningFcn(hObject, eventdata, handles, varargin)
% Choose default command line output for interface
handles.output = hObject;

% Update handles structure

%--------------Variable initialisation 
% Flag initialisation 
handles.StopFlag=0;
handles.CurrentFrame='';
handles.openAviFlag=0;
handles.analyseFlag = 0;
handles.exportFlag = 0;

handles.FigureSavePath = 'D:\usr\www\figures\';
% calculate parameters 
handles.FirstFrame='';
handles.LastFrame='';
handles.RefFrame='';
setTextBox(handles);
handles.numfigure=1;
handles.SeuilAmplitude=0.5;
handles.SeuilRupture=0.1;
handles.AutoSelect=1;
handles.MedianSize=1;



guidata(hObject, handles);




%handles.output
% UIWAIT makes interface wait for user response (see UIRESUME)
% uiwait(handles.figure1);
% H=axes('unit','normalized','position',[0,0,1,1],'visible','off');
% set(gcf,'currentaxes',H);

% --- Outputs from this function are returned to the command line.
function varargout = interface_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% Get default command line output from handles structure
varargout{1} = handles.output;

% Ouvrir le fichier AVI
function ouvrir_avi_Callback(hObject, eventdata, handles)
[handles.AviFileName, handles.Repertoire] = uigetfile('*.avi', 'Ouvrir un fichier AVI,SVP');
if handles.AviFileName~=0 % si le fichier n'est pas vide , ouvre le fichier
    FullFilePath=fullfile(handles.Repertoire,handles.AviFileName); % combine le nom de fichier et la Repertoire pour chemin d'access de fichie
    
    %check if the video name format AAAA-BB.avi
    if(size(handles.AviFileName,2)~=11)
        errorMsg('Veuillez verifier le nom de ficher avi est sous le format "AAAA-BB.avi"(AAAA est nom de gene, BB est numero de video)');
        return;
    end
        handles.ExportData.numgen =handles.AviFileName(1:4);
        handles.ExportData.numvideo = handles.AviFileName(6:7);
        
    disp(FullFilePath);
%     if (handles.openAvi==1)     %clear previous opened avi
%         clear handles.mov;
%     end
    handles.mov=aviread(FullFilePath);   %read the avi file   
    handles.openAviFlag=1;
    VideoInfo=aviinfo(FullFilePath);
    handles.SampleFrequence=VideoInfo.FramesPerSecond;%variable gobal frequence d'echantillionnage
    set(handles.frequance_echant,'String',num2str(handles.SampleFrequence)); 
    handles.LengthVideo=VideoInfo.NumFrames;

    imshow(handles.mov(1).cdata,'parent',handles.video);
    handles.CurrentFrame=1;
    handles.RefFrame=1;
    handles.FirstFrame=1;
    handles.LastFrame=handles.LengthVideo;
    setTextBox(handles);

    guidata(hObject,handles);
else % si le fichier est vide , retourner 
    errorMsg('le ficher avi est vide!');
    return;
end

function slider1_Callback(hObject, eventdata, handles)
if(~handles.openAviFlag )
    errorMsg('Pas de video ouverte!');
    return;
end
    if get(hObject,'Value')==0
        handles.CurrentFrame=1;
    else
        handles.CurrentFrame=round((get(hObject,'Value'))*handles.LengthVideo);
    end
    dispCurrentFrame(handles,hObject);

function slider1_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function backFrame_Callback(hObject, eventdata, handles)
if(~handles.openAviFlag )
    errorMsg('Pas de video ouverte!');
    return;
end
    
    if handles.CurrentFrame==1
        return;
    else
        handles.CurrentFrame=handles.CurrentFrame-1;
        dispCurrentFrame(handles,hObject);
        
    end
function nextFrame_Callback(hObject, eventdata, handles)
if(~handles.openAviFlag )
    errorMsg('Pas de video ouverte!');
    return;
end
    if handles.CurrentFrame==handles.LengthVideo
        return;
    else
        handles.CurrentFrame=handles.CurrentFrame+1;
        dispCurrentFrame(handles,hObject);
    end
function Analyser_Callback(hObject, eventdata, handles)
clc;
%-----test if the video is open
if(~handles.openAviFlag)
   errorMsg('Pas de video ouverte!');
   return;
end
%-----end of test

fDelFigure(1);% delete current displayed figure

step=str2double(get(handles.stepsizetag,'string'));
mov=handles.mov(handles.FirstFrame:step:handles.LastFrame); %copier le ficher avi dans le handles

if (handles.AutoSelect) %si la case a cocher d'Automatic Seletion of Reference est coche,la trame de reference sera choisie automatiquement
   RefFrameNum=mimage_moyenne(mov);
else
   RefFrameNum=handles.RefFrame;
end

AnalyseZone=fAnalyseZoneSelection(mov(RefFrameNum));
xlabel(strcat('Numero de trame: ',num2str(RefFrameNum)));%display the number of reference image on the figure

strCalParam.Corrl=CorrelationImage(mov,AnalyseZone,RefFrameNum);
if(handles.MedianSize~=1) %filtrage median
    strCalParam.Corrl=medfilt1(strCalParam.Corrl,handles.mediansize);    
end

strCalParam.Fe=handles.SampleFrequence/step;                                    %FPS de vid��o
strCalParam.SeuilAmplitude=handles.SeuilAmplitude;
strCalParam.SeuilRupture=handles.SeuilRupture*handles.LengthVideo/2; %seuil pour d��tecter la rupture 
if (get(handles.afficheSeq,'Value'))
    strCalParam.afficheSeq=get(handles.afficheSeq,'Value');
else 
    strCalParam.afficheSeq=0;
end

if (get(handles.derivationtag,'value')== get(handles.derivationtag,'Max'))
   strCalParam.derivation=1;
else
    strCalParam.derivation=0;
end

handles.strResult=fPostTreatment(strCalParam);

strCalParam.AviFile=handles.AviFileName;%nom et chemin d'access de fichier avi
strCalParam.Repertoire=handles.Repertoire;
handles.hFig=fResultVisualisation(strCalParam.Corrl,handles.strResult);

%------display information in the main window
    set(handles.nomvideotag,'String',handles.AviFileName);
    set(handles.lengthtag,'String',handles.LengthVideo);
    set(handles.AffFrequencetag,'String',handles.strResult.sFrequence);
    set(handles.periodemoytag,'String',handles.strResult.periode_moyenne);
    set(handles.freqbattag,'String',num2str(handles.strResult.BattementParMinute));
    set(handles.nbperiodetag,'String',handles.strResult.nombrePeriode);
    if(handles.strResult.Anomalie)
        set(handles.anomalietag,'String','Anomalie d��tect��e');
    else
         set(handles.anomalietag,'String','Non d��tect��e');
    end
%-------end display
    handles.exportFlag=1;
    handles.analyseFlag=1;
    guidata(hObject,handles);
    
function CurrentFrameTag_Callback(hObject, eventdata, handles)
if (~handles.openAviFlag)
    errorMsg('Pas de video ouverte!');
    return;
elseif ((str2double(get(hObject,'String'))>handles.LengthVideo)|(str2double(get(hObject,'String'))<1))
    WarningBox(hObject, handles);                 
    return;
end
  handles.CurrentFrame=(str2double(get(hObject,'String')));
  dispCurrentFrame(handles,hObject);  %returns contents of edit1 as a double
guidata(hObject, handles);
function CurrentFrameTag_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function play_Callback(hObject, eventdata, handles)
if (~handles.openAviFlag)
    errorMsg('Pas de video ouverte!');
    return;
end
TempPause=1/handles.SampleFrequence;
i = handles.CurrentFrame;% : handles.LengthVideo
   while(handles.StopFlag==0&&i<handles.LengthVideo) 
       handles.CurrentFrame=i;
       i=i+1;
       dispCurrentFrame(handles,hObject);
       pause(TempPause);

end
function Stop_Callback(hObject, eventdata, handles)
% hObject    handle to Stop (see GCBO)
handles.StopFlag=1;
disp('alors@!');
disp(handles.StopFlag);
guidata(hObject,handles);
function Stop_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to Stop (see GCBO)
handles.StopFlag=1;
guidata(hObject,handles);
function FirstFrameTag_Callback(hObject, eventdata, handles)
    % test si la valeur saisie est dans le cadre des trames totales
    
if (~handles.openAviFlag)
    errorMsg('Pas de video ouverte!');
    return;
end
    hObjValue=str2double(get(hObject,'String'));
    if(hObjValue>handles.LengthVideo-1||hObjValue<1||hObjValue>handles.LastFrame)
        WarningBox(hObject, handles);
        return;
    end

    % test si la trame de reference est inferieur a la 1er trame
    if handles.CurrentFrame<handles.FirstFrame
       handles.CurrentFrame=hObjValue;
       dispCurrentFrame(handles,hObject);
    end
    handles.FirstFrame=hObjValue;
guidata(hObject,handles);
function FirstFrameTag_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
function LastFrameTag_Callback(hObject, eventdata, handles)
if(~handles.openAviFlag )
    errorMsg('Pas de video ouverte!');
    return;
end

    hObjValue=str2double(get(hObject,'String'));%validation of the input
    if(hObjValue>handles.LengthVideo||hObjValue<2||hObjValue<handles.FirstFrame)
        WarningBox(hObject,handles);
        return;
    end
    if handles.CurrentFrame>hObjValue
       handles.CurrentFrame=hObjValue;
       dispCurrentFrame(handles,hObject);
    end
    handles.LastFrame=hObjValue;
guidata(hObject,handles);
function LastFrameTag_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function setTextBox(handles)
    setFirstFrame(handles.FirstFrameTag, handles);
    setLastFrame(handles.LastFrameTag, handles);
    setCurrentFrame(handles.CurrentFrameTag, handles);   
function setFirstFrame(hObject, handles)
    set(hObject,'String',num2str(handles.FirstFrame));% display the first frame number in the text box
guidata(hObject, handles);
function setLastFrame(hObject, handles)
    set(hObject,'String',...
    num2str(handles.LastFrame));% display the last frame number in the text box
guidata(hObject, handles);
function setCurrentFrame(hObject, handles)
    set(hObject,'String',num2str(handles.CurrentFrame));% display the current frame number in the text box
guidata(hObject, handles);
function SeuilRupturesSlider_Callback(hObject, eventdata, handles)
value=get(hObject,'Value');
if value>=0&&value<=0.1
    handles.SeuilRupture=(value+0.1);
else    
    handles.SeuilRupture=value/2;
end
set(handles.seuilrupturetag,'String',num2str(handles.SeuilRupture*100));% display the first frame number in the text box
guidata(hObject, handles);
function SeuilRupturesSlider_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
set(hObject,'value',0.1);
function AutoSelectRefTag_Callback(hObject, eventdata, handles)
if(hObject == handles.AutoSelectRefTag)  %si la case a cocher d'Automatic Seletion of Reference est coche,la trame de reference sera choisie automatiquement
    handles.AutoSelect=1;
else
    handles.AutoSelect=0;
end
function seuilamplitudetag_Callback(hObject, eventdata, handles)
function seuilamplitudetag_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function seuilrupturetag_Callback(hObject, eventdata, handles)
function seuilrupturetag_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function seuilamplitude_Callback(hObject, eventdata, handles)
value=(get(hObject,'Value'));
if value>=0&&value<=0.05
    handles.SeuilAmplitude=value+0.05;
else
    handles.SeuilAmplitude=value;
end
set(handles.seuilamplitudetag,'String',...
    num2str(handles.SeuilAmplitude*100));% display the first frame number in the text box
guidata(hObject, handles);
function seuilamplitude_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
set(hObject,'value',0.5);
function frequance_echant_Callback(hObject, eventdata, handles)
handles.SampleFrequence = str2double(get(hObject,'String'));
guidata(hObject, handles); %//pour sauvgarder les donn��es dans le handel
function frequance_echant_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function stepsizetag_Callback(hObject, eventdata, handles)
function stepsizetag_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function mediumsizetag_Callback(hObject, eventdata, handles)
handles.mediansize=str2num(get(hObject,'string'));
guidata(hObject, handles);     
function mediumsizetag_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function afficheSeq_Callback(hObject, eventdata, handles)
function WarningBox(hObject, handles)%this function check the case of the input text box statement and display the warning box
    switch hObject
        case handles.currentFrameTag
            FirstFrame=1;
            LastFrame=handles.LengthVideo;
            setCurrentFrame(hObject, handles);
        case handles.FirstFrameTag
            FirstFrame=1;
            LastFrame=handles.LastFrame;
            setFirstFrame(hObject, handles);
        case handles.LastFrameTag
            FirstFrame=handles.FirstFrame;
            LastFrame=handles.LengthVideo;
            setLastFrame(hObject, handles);
    end
    questdlg(['Please enter a value between ',int2str(FirstFrame) ,'and ',int2str(LastFrame)],...
                         'Incorrect input number',...
                         'OK','OK');
function dispInfo(handles,strResult)
    set(handles.nomvideotag,'String',handles.AviFileName);
    set(handles.lengthtag,'String',handles.LengthVideo);
    set(handles.AffFrequencetag,'String',handles.strResult.strfrequence);
    set(handles.periodemoytag,'String',handles.strResult.periode_moyenne);
    set(handles.freqbattag,'String',num2str(60*1000/handles.strResult.periode_moyenne));
    set(handles.nbperiodetag,'String',handles.strResult.nombreperiode);
    if(handles.strResult.Anomalie)
        set(handles.anomalietag,'String','Anomalie d��tect��e');
    else
         set(handles.anomalietag,'String','Non d��tect��e');
    end
function export_Callback(hObject, eventdata, handles)
 if (~handles.analyseFlag)
        errorMsg('Veuillez analyser d abord!');
        return;
 end
 
 if(~handles.exportFlag)
    errorMsg('Exporte dupliqu��!')
    return;
 end

 [status figPath]=figExport(handles.hFig,handles.FigureSavePath,strcat(handles.ExportData.numgen,'_',handles.ExportData.numvideo));
 if (~status)
     errorMsg('Figure export echoue!');
   return;
 end 
query = strcat('INSERT INTO `analyse` (`id`, `numgene`,`numvideo`,`date`,`time`,`nbbattement`,`batparmin`,`frequence`,`duree`,`anomalie`,`figpath`,`videopath`,`videofrequence`,`comment`) VALUES (NULL,''', handles.ExportData.numgen, ''',''', handles.ExportData.numvideo, ''', CURDATE()',', CURTIME(),''',num2str(handles.strResult.nombrePeriode), ''',''', num2str(round(handles.strResult.BattementParMinute)), ''',''',  num2str((handles.strResult.frequence(1))), ''',''', num2str(round(handles.strResult.periode_moyenne)), ''',''', num2str(handles.strResult.Anomalie), ''',''', strrep(figPath,'\','\\') ,''',''', strrep(fullfile(handles.Repertoire,handles.AviFileName),'\','\\'), ''',''', num2str(handles.SampleFrequence),''',NULL)');
disp(query);
hResult=dbinsert(query);
if(~hResult)
    errorMsg('Data export echoue');
    return;
end
handles.exportFlag=0;
guidata(hObject,handles);



% --- Executes on mouse press over axes background.
function video_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to video (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


