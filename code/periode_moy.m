function periode_moyenne=periode_moy(periode)
% Function : periode_moy.m
% Author : Abdelatif Serir et SONG Ke
% Date : 02/2008
% Description : cette fonction prend le vector qui contient la dur��e de
%               tout les periodes (unit��: ms) et calcule la dur��e moyenne
% Argument :   vector contenant la dur��e de tout les periodes (unit��: ms)
% Copyright 2007-2008 LAGIS 
[n,x]=hist(periode);
periode_moyenne=sum(n.*x)/(sum(n)); 
