function vectorderviated=derivation(vfrequence,Fe)

vectorderviated=zeros(1,size(vfrequence,2));
for i = 2:(size(vfrequence,2)-1)
    vectorderviated(i)=(vfrequence(i+1)-vfrequence(i-1))/(2*Fe);
end