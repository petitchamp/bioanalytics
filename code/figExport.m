function [STATUS , path]=figExport(hFig,path,filename)
% -------------------------------------------------------------------------
% Author : SONG Ke
% The funtion save the .fig and PNG thumbnail file  of hFig in the specified path with a with
% composed by the current time stamp and filename to avoid name conflit
% [Parameter:]
% [hFig : graphic handle of the result figure]
% [path : path to save the figure file and the thumbnail, by default it is like this: c:\abc\, the \ at last is not important] 
% [filename : the file name of the avi file]
% Copyright SONG KE 2008
%input validation test
%--------------------------
if(isempty(hFig)||isempty(path)||isempty(filename))
    disp('figExport.m:input error in fig export');
    STATUS=0;
    return;
end
%end input validation test
if(~strcmp(path(size(path,2)),'\')); %check if the path string is end with "\", if not ,add one to compelete the path
    path=strcat(path,'\');
end
timeId=num2str(now);

status=chkandmkdir(path);
path=strcat(path, filename, timeId);
saveas(hFig,strcat(path, '.fig'));

if(~status)
    disp('figExport.m :path check error');
    return;
end

imagePath=strcat(path, '.png');
saveas(hFig,imagePath);
STATUS=1;
% print -dpng -r50 temp;    %generate 320x240 thumbnail of figure
% [STATUS,MESSAGE,MESSAGEID]= movefile('temp.png',imagePath);
% if (STATUS ==0)
%     errorMsg(strcat(STATUS,MESSAGE,MESSAGEID));
% end
