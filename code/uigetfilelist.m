function [wkdir filelist]=uigetfilelist(filetype)
% Function : uigetfilelist.m
% Author : SONG Ke
% Date : 03/2008
% Description : this function receives the File Type and return the path
% and a list of all the files with the file type received
% Argument : filetype: a string that specifies the file type
% Copyright 2008 SONG Ke
wkdir=uigetdir('D:\Desktop\ProjetM2\avi','Choisissez une dossier');
filelist = dir(strcat(wkdir,'\*.',filetype));