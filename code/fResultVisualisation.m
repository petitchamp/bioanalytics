function fig1= fResultVisualisation(Corrl,strResult)

% Corrl
% axefreq
% fftCorrl
% periode
% periode_moyenne


% nbPeriode=size(periode,2);
% -------------------------------------------------------------------------
% affichage des figure
% -------------------------------------------------------------------------
fig1=figure(1);
    subplot(2,3,[2 3]);
    % plot(axetemps,Corrl);
    plot(Corrl);
    xlabel('Trame(s)');
    ylabel('Coefficent Correlation');
    grid on;
    hold on;
    plot(Corrl,'.');
    
    subplot(2,3,[5 6]);
    plot(strResult.axefreq,strResult.fftCorrl);
    xlabel(strcat('Les fr��quences sont: ',strResult.sFrequence,'Hz'));
    ylabel(strcat('Spectre de puissance'));
    grid on;

%afficher les periode
    subplot(2,3,4);
    hist(strResult.Periode,strResult.nombrePeriode);%histograme de distribution de duree de periode
    title(strcat('Distribution des periodes '));
    xlabel(strcat('Periode (ms) moyenne:',num2str(strResult.periode_moyenne),' Periode Totale:',num2str(strResult.nombrePeriode)));%afficher la duree moyenne et le nombre de periode dans le video
    ylabel('Population');
    grid on;
    