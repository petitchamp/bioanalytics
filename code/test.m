function varargout = test(varargin)
% This UI Program is dedicated to automatic measurement of cardiac diameter infomation from the 
% Video. To extract the needed information, user needs to specify the axis of the cardiac tube and 
% A point along the axis which specifies the position of radiance to be measured
% Last Modified by GUIDE v2.5 29-Jun-2008 22:25:06
% Copyright SONG KE 2008

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @test_OpeningFcn, ...
                   'gui_OutputFcn',  @test_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before test is made visible.
function test_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
handles.hSampleLine=0;

guidata(hObject, handles);

% UIWAIT makes test wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = test_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;

% --- Executes on button press in OpenButton.
function OpenButton_Callback(hObject, eventdata, handles)
[handles.AviFileName, handles.Repertoire] = uigetfile('*.avi', 'Ouvrir un fichier AVI,SVP');
if handles.AviFileName~=0 % si le fichier n'est pas vide , ouvre le fichier
    % combine le nom de fichier et la Repertoire pour chemin d'access de fichie
    FullFilePath=fullfile(handles.Repertoire,handles.AviFileName); 
    
    handles.avi=aviread(FullFilePath);%read the avi file   
    handles.openAviFlag=1;
    VideoInfo=aviinfo(FullFilePath);
    handles.SampleFrequence=VideoInfo.FramesPerSecond;%variable gobal frequence d'echantillionnage
    handles.LengthVideo=VideoInfo.NumFrames;

    % Display the first frame on the screen
    % Use Parent parameter to change image displayed in first axis
    imshow(handles.avi(1).cdata,'parent',handles.axes1); 
    % set(ImageHandle,'ButtonDownFcn',@(button_down(src,event)) disp(src));

    % Create the cardiac tube axe indication line
    % ----------------------------------------------------------------------------------------------
    handles.AxeLine=imdistline(handles.axes1);%imline(handles.axes1,[]);
    LineAPI=iptgetapi(handles.AxeLine);

    fcn = makeConstrainToRectFcn('imline',get(gca,'XLim'),get(gca,'YLim')); % Constrain function that limits the ends inside of the iamge
    LineAPI.setDragConstraintFcn(fcn);
    LineAPI.setLabelTextFormatter('0');
    LineAPI.addNewPositionCallback(@MovePosition);

    % Create the sample line position indication point
    % ----------------------------------------------------------------------------------------------
    handles.PointHandle = impoint(handles.axes1,10,20);
    PointAPI=iptgetapi(handles.PointHandle);
    PointAPI.setString('Move the point to indicate the sample line position');
    PointConstrainfcn = makeConstrainToRectFcn('impoint',get(gca,'XLim'),get(gca,'YLim'));
    PointAPI.setPositionConstraintFcn(PointConstrainfcn);   
    PointAPI.addNewPositionCallback(@MovePoint);
    guidata(hObject, handles);
else
    disp('vide avi ficher');
    return;
end
    

function MovePosition(pos)
handles=guidata(gcbo);
LineAPI=iptgetapi(handles.AxeLine);
% if(pos(1,1)-pos(2,1)==0)
%     angle=90;
% else
%     tangent=(pos(1,2)-pos(2,2))/(pos(1,1)-pos(2,1));
%     radians=atan(tangent);
%     angle=-radians/pi*180;
%     if(angle<0)
%         angle=angle+180;
%     end    
% end
LineAPI.setLabelTextFormatter(strcat(num2str(LineAPI.getAngleFromHorizontal())));

function MovePoint(position)
handles=guidata(gcbo);
PointAPI=iptgetapi(handles.PointHandle);
PointAPI.setString(strcat(num2str(round(PointAPI.getPosition()))));

% --- Executes on button press in DiametreAnalyse.
function DiametreAnalyse_Callback(hObject, eventdata, handles)
    
PointAPI=iptgetapi(handles.PointHandle);
LineAPI=iptgetapi(handles.AxeLine);
pointPosition=PointAPI.getPosition();
angle=180-LineAPI.getAngleFromHorizontal();
coef=PerpendicularLine(pointPosition(1),pointPosition(2),angle);

% find 2 end of the line in the image area
Ysize=size(handles.avi(1).cdata,1);
Xsize=size(handles.avi(1).cdata,2);
X1=CalculateX(0,coef);
if(X1<0)
     X1=1;% begins from 1 because matrix index begins from 1
     Y1=CalculateY(coef,X1);
else
     Y1=1;
     X1=CalculateX(1,coef);
end
X2=CalculateX(Ysize,coef);
if(X2>Xsize)
    X2=Xsize-1;
    Y2=CalculateY(coef,X2);
else
    Y2=Ysize;
    X2=CalculateX(Y2,coef);
end

% Draw the sample line, if it doesn't exist before ,create it, otherwise reposition it
if(~handles.hSampleLine)
    handles.hSampleLine=imline(handles.axes1,[X1 Y1;X2 Y2]);
else
    apiSampleLine=iptgetapi(handles.hSampleLine);
    apiSampleLine.setPosition([X1 Y1;X2 Y2]);
end


% Calculate the discreted points coordinates of the sample line
SampleLine=(SampleLineCoordinate(round([X1,Y1]),round([X2,Y2])));

% Extract the points from the video frame
for iNbFrame=1:handles.LengthVideo
    im=handles.avi(iNbFrame).cdata(:,:,1);
    for i=1:size(SampleLine,2)
        imextra(i)=double(im(SampleLine(2,i),SampleLine(1,i)));
    end
            if (max(imextra)>255)
            disp(iNbFrame);
        end 
    extracted(iNbFrame,:)=(EqualisationHist(imextra));
    figure(1);
    subplot(1,3,1);
    plot(extracted(iNbFrame,:));
    pause(.1);
end

subplot(1,3,2:3);
% imshow(im);
surface(extracted);
