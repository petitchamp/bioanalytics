function coefficient= PerpendicularLine2(x,y,angle)
% function : PerpendicularLine.m
% Author : Song Ke
% Date : 11/2007
% Description : This function takes coordinate(x,y) of a point on a 2D line and the
% angle in degree between the Axes X and the line and return the coifficients of
% formula Ay+Bx+C=0 of its peroendicular line
% Parameter x,y : the coordinates of a 2D Point
%                   angle : angle between  the 2D line and x axes. Units in
%                                unite in degree, valid range is from 0 to
%                                180

% Copyright SONG KE 2008
if ((angle>180)||(angle<0))
    disp('Invalid angle input. should between 0 and 180');
    errorMsg('Invalid angle input. should between 0 and 180');
    coefficient=0;
    return;
end
if(angle==0)
      coefficient(1)=0;
      coefficient(2)=1;
      coefficient(3)=-x;
    
elseif(angle==90)
      coefficient(1)=1;
      coefficient(2)=0;
      coefficient(3)=-y;   
    
else
    coefficient(1)=1;
    coefficient(2)=1/tan(angle/180*pi);
    coefficient(3)=-(y-coefficient(1)*x); 
end