function strReturn=peakdet(VectFFT,axefreq,nbDecalage,seuilAmplitude)
% Function : peakdet.m
% Author : Abdelatif Serir et SONG Ke
% Date : 11/2007
% Description : cette fonction retourne les 3 1er valeurs de FFT qui est
%               superieur �� seuilAmplitude*peakmax 
% Argument : VectFFT            Vector qui contient la transformee de fourrier
%            seuilAmplitude     valeur entre 0 et 1, le seuil pour limiter
%                               la recheche de peak
% Copyright 2007-2008 LAGIS 

length=size(VectFFT,2);
freqSearch=VectFFT(nbDecalage:length);% seuil est pour eliminer les peaks pr��s de z��ro


maxpeak=max(freqSearch);
maxintial=maxpeak;
j=1;
while(maxpeak>=seuilAmplitude*maxintial&&j<4)% nous ne prennons compte que les 1er 3 frequences principales
        coorpeak(j)=find(freqSearch==maxpeak); %sauvgarder la coordonnee de peak
        % nuller le peak pour chercher le 2eme peak
        i=coorpeak(j);
        if(coorpeak(j)<3)
            i=i+2;
        end
        freqSearch(i-2:i+2)=0;
    maxpeak=max(freqSearch);% renouveller la valeur max
    j=j+1;
end

% for i=1:size(coorpeak,2) % trouver les valeurs des frequences correspendent aux peaks  
    strReturn.frequence=axefreq(coorpeak+nbDecalage-1);
% end


%detection of anomalie, the 
amplitude_max=max(freqSearch); % le peak max dans la zone 

if(isempty(find(VectFFT>amplitude_max*seuilAmplitude,1)))
    strReturn.Anomalie=0;
else
    strReturn.Anomalie=0;
end