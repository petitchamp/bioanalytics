function fftSignal=FFTCorrelation(signal)
% Function : FFTCorrelation.m
% Author : Abdelatif Serir et SONG Ke
% Date : 11/2007
% Description : cette fonction calcule la fft de signal entr��,decale la
%               fft en utilisant fftshift() et rend tous les valeurs ��
%               positives avec abs()
% Argument : signal   Vector de signal �� calculer le fft
% Copyright 2007-2008 LAGIS 
length=size(signal,2);
fftSignal=abs(fftshift(fft(signal)));
fftSignal=fftSignal(length/2+1:length);