function dispCurrentFrame(handles,hObject)        
% function : dispCurrentFrame.m
% Author : Song Ke
% Date : 11/2007
% Description : Cette fonction recevoie le handles de la GUI et afficher la
%               trame courante dans la zone vid��o dans la GUI et afficher
%               le num��ro de la trame courante dans les contr?lers
%               n��cessaire
% Copyright 2007-2008 LAGIS USTL

frame=handles.mov(handles.CurrentFrame).cdata;
frame=histoeq(frame(:,:,1),'adjust');%frame histogram equalized
imshow(frame,'parent',handles.video);
%         save the datum in the handle object and then save the handle in
%         the buffer of the interface

set(handles.CurrentFrameTag,'String',num2str(handles.CurrentFrame));% display the current frame number in the text box
set(handles.slider1,'Value',(handles.CurrentFrame/handles.LengthVideo));

guidata(hObject,handles);