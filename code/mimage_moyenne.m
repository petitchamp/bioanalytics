function j=mimage_moyenne(mov)
% Function : mimage_moyenne.m
% Author : Abdelatif Serir et SONG Ke
% Date : 01/2008
% Description : Cette fonction d��t��rmine l'image qui a la plus petit moyenne.
%               il calcule la valeur moyenne de 50% video et retourne l'indice 
%               de la frame qui a la moyenne minmumminmum 
% Argument : mov        Type:struct,la sequence de vid��o acquise
% Copyright 2007-2008 LAGIS 

mimage_moyenne=zeros(1,size(mov,2)*0.5);
for i=1:size(mov,2)*0.5
   image=mov(i).cdata;
   mimage_moyenne(i)=mean(mean(histoeq(image(:,:,1),'adjust')));
end
j=find(mimage_moyenne==min(mimage_moyenne));

