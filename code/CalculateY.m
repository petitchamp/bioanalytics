function y=CalculateY(coefficient,x)
% function : PerpendicularLine.m
% Author : Song Ke
% Date : 11/2007
% Description : This function takes the tangent, the x value and the
% intercept of a 2D line and then returns the corresponding y value
% Parameter coefficient : 1X2 vector containing tangent(coefficient(1)) and 
%                                        the intercept(coefficient(2)) of 2D line
%                                   x : the x value
% Copyright SONG KE 2008

y=coefficient(1)*x+coefficient(2);
