function  status=chkandmkdir(repertoire)
if(~exist(repertoire,'dir')) %check if the directory exist
          [status,message,messageid]=mkdir(repertoire);
        if(~status)  % if the creation failed, quit
            disp('chkandmkdir ',repertoire,' error: matlab erreur message:',message,'matlab erreur ID:',messageid);
            return;
        end
else
    status=1;
end