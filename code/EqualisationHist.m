function out=EqualisationHist(in)
% This function takes a vector or a matrix and equalize it like as an image. that is to
% say
% For vector : set the max value to 255 and the min value to 0. Other values are
% between them.
% For matrix : each line is treated as la vector

for i=1:size(in,1)
    maxValue=max(in(i,:));
    minValue=min(in(i,:));
    Amplitude=maxValue-minValue+1;
    if(Amplitude==0)
        continue;
    elseif(maxValue>255)
        disp(maxValue);
    end
    for j=1:size(in,2)
      out(i,j)=round(255*(in(i,j)-minValue-1)/Amplitude);
    end
end