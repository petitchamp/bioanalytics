function fRectangle(corrdinate)
% this function draw a rectangle on the current axi object
% the default line width is 2 point
coefMatrix=[0,0;
            1,0;
            1,1;
            0,1;
            0,0];
firstPoint=corrdinate(1:2);
wh=corrdinate(3:4);%width and height of the rectangle
for i=1:4
    line(firstPoint(1)+coefMatrix(i:i+1,1)*wh(1),firstPoint(2)+coefMatrix(i:i+1,2)*wh(2),'LineWidth',2)
    % line(firstPoint+wh*diag(coefMatrix(i)),firstPoint+wh*diag(coefMatrix(i+1)));
end
