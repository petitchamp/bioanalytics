function hResult=dbconnection(query)
conn = database('bioinfo','','');
 if (conn.Handle==0)
    errorMsg('Database connection error!');
    hResult=0;
     return;
 end
% query = 'NULL, CURRENT_TIMESTAMP';
% Read data from database.
cursor = exec(conn,query);
if (~isempty(cursor.Message))
    errorMsg('Data insertion echoue!');
    hResult=0;
    return;
end

% Close database connection.
close(conn)
    hResult=1;

