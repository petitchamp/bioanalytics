function LineCoordinate=SampleLineCoordinate(Point1,Point2)
% This fucntion takes the coordinates of 2 points and output the a 2XN 

if(size(Point1,2)~=2)||((size(Point2,2)~=2))
    return;
end

if(Point1(1)==Point2(1))
    %   case that the point belong to a line that satisfies the equation
    %   y=N where N is a real number
    Size=abs(Point1(2)-Point2(2))+1;
    YStep=-(Point1(2)-Point2(2))/abs((Point1(2)-Point2(2)));
    LineCoordinate=zeros(2,Size);
    LineCoordinate(1,:)=Point1(1);
    LineCoordinate(2,:)=Point1(2):YStep:Point2(2);
    return;
elseif(Point1(2)==Point2(2))
    %   case that the point belong to a line that satisfies the equation
    %   x=N where N is a real number
    Size=abs(Point1(1)-Point2(1))+1;
    XStep=-(Point1(1)-Point2(1))/abs((Point1(1)-Point2(1)));
    LineCoordinate=zeros(2,Size);
     LineCoordinate(1,:)=Point1(1):XStep:Point2(1);    
     LineCoordinate(2,:)=Point1(2);
    return;
else
    coefficient(1)=((Point1(2)-Point2(2))/(Point1(1)-Point2(1)));
    coefficient(2)=Point1(2)-coefficient(1)*Point1(1);
    XStep=-(Point1(1)-Point2(1))/abs((Point1(1)-Point2(1)));
    YStep=-(Point1(2)-Point2(2))/abs((Point1(2)-Point2(2)));
    YCooridnate=Point1(2);
    ii=1;
    for X=Point1(1):XStep:Point2(1)
        while(YCooridnate<=CalculateY(coefficient,X))
            LineCoordinate(1,ii)=X;
            LineCoordinate(2,ii)=YCooridnate;
            YCooridnate=YCooridnate+YStep;
            ii=ii+1;
        end
    end
end

StreetDistance=abs(Point1(1)-Point2(1))+abs(Point1(2)-Point2(2));
% LineCoordinate=zeros(2 ,StreetDistance);
% disp(LineCoordinate);